import java.util.Scanner;

public class NewXoxo2 {

    private static char[][] XO = new char[3][3];
    private static Scanner kb = new Scanner(System.in);
    private static int End = 0;
    private static int row;
    private static int col;
    private static int row1 = 0;
    private static int col1 = 0;
    private static int sum = 1;
    private static char turn;

    private static void showSay() {
        System.out.println("Welcome to OX Game");
    }

    private static void inputTable(char[][] XO) {
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                XO[j][k] = '_';
            }
        }
    }

    private static void showTable() {
        System.out.println();
        for (int h = 0; h < 3; h++) {
            for (int k = 0; k < 3; k++) {
                System.out.print(XO[h][k] + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    private static void inputRowcol() {
        System.out.print("Please input row,col : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        checkInputXo();
    }

    private static void inputRow() {
        System.out.print("Please input row : ");
        row = kb.nextInt() - 1;
        checkInputXo();
    }

    private static void inputCol() {
        System.out.print("Please input col : ");
        col = kb.nextInt() - 1;
        checkInputXo();
    }

    private static void inputOx() {
        if (sum % 2 == 0) {
            System.out.println("turn O");
            inputRowcol();
        } else {
            System.out.println("turn X");
            inputRowcol();
        }
    }

    private static void checkInputXo() {
        if ((row > 2 || row < 0) && col < 3) {
            System.out.println("Row : Please input num less than 3");
            inputRow();
        } else if ((col > 2 || col < 0) && row < 3) {
            System.out.println("Col : Please input num less than 3");
            inputCol();
        } else if (col > 2 && row > 2) {
            System.out.println("Row and Col : Please input num less than 3");
            inputOx();
        } else if (XO[row][col] == ('O') || XO[row][col] == ('X')) {
            System.out.println("NOT Empty!!");
            inputOx();
        } else {
            addXo();
        }
    }

    private static void addXo() {
        if (sum % 2 == 0) {
            XO[row][col] = ('O');
        } else {
            XO[row][col] = ('X');
        }
    }

    private static void findTurn() {
        if (sum % 2 == 0) {
            turn = 'O';
        } else {
            turn = 'X';
        }
    }

    private static void checkOxwin() {
        if (turn == 'O') {
            End = 1;
        } else if (turn == 'X') {
            End = 2;
        }
    }

    private static void checkwin() {
        findTurn();
        if (XO[row1][col1] == (turn) && XO[row1][col1 + 1] == (turn) && XO[row1][col1 + 2] == (turn)
                || XO[row1][col1] == (turn) && XO[row1 + 1][col1] == (turn) && XO[row1 + 2][col1] == (turn)
                || XO[row1][col1] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 2][col1 + 2] == (turn)
                || XO[row1 + 1][col1] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 1][col1 + 2] == (turn)
                || XO[row1 + 2][col1] == (turn) && XO[row1 + 2][col1 + 1] == (turn) && XO[row1 + 2][col1 + 2] == (turn)
                || XO[row1][col1 + 1] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 2][col1 + 1] == (turn)
                || XO[row1][col1 + 2] == (turn) && XO[row1 + 1][col1 + 2] == (turn) && XO[row1 + 2][col1 + 2] == (turn)
                || XO[row1][col1 + 2] == (turn) && XO[row1 + 1][col1 + 1] == (turn) && XO[row1 + 2][col1] == (turn)) {
            checkOxwin();
        }
    }

    private static void checkDraw() {
        if (sum == 9 && End == 0) {
            End = 3;
        }
    }

    private static void printOwin() {
        if (End == 1) {
            System.out.println("O Win");
        }
    }

    private static void printXwin() {
        if (End == 2) {
            System.out.println("X Win");
        }
    }

    private static void printDraw() {
        if (End == 3) {
            System.out.println("Draw");
        }
    }

    public static void main(String[] args) {
        showSay();
        inputTable(XO);
        showTable();
        while (End == 0) {
            inputOx();
            showTable();
            checkwin();
            checkDraw();
            sum++;
        }
        printOwin();
        printXwin();
        printDraw();
    }
}
