import java.util.Scanner;

public class test {

    private static char[][] XO = new char[3][3];
    private static Scanner kb = new Scanner(System.in);
    private static int End = 0;
    private static int row;
    private static int col;
    private static int row1 = 0;
    private static int col1 = 0;
    private static int sum = 1;

    private static void showSay() {
        System.out.println("Welcome to OX Game");
    }

    private static void inputTable(char[][] XO) {
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 3; k++) {
                XO[j][k] = '_';
            }
        }
    }

    private static void showTable() {
        System.out.println();
        for (int h = 0; h < 3; h++) {
            for (int k = 0; k < 3; k++) {
                System.out.print(XO[h][k] + " ");
            }
            System.out.println();
        }
    }

    private static void inputRowcolnew() {
        System.out.print("Please input row,col : ");
        row = kb.nextInt() - 1;
        col = kb.nextInt() - 1;
        checkInputXO();
    }

    private static void inputRownew() {
        System.out.print("Please input row : ");
        row = kb.nextInt() - 1;
        checkInputXO();
    }

    private static void inputColnew() {
        System.out.print("Please input col : ");
        col = kb.nextInt() - 1;
        checkInputXO();
    }

    private static void inputOX() {
        if (sum % 2 == 0) {
            System.out.println("turn O");
            inputRowcolnew();
        } else {
            System.out.println("turn X");
            inputRowcolnew();
        }
    }

    private static void checkInputXO() {
        if ((row > 2 || row < 0) && col < 3) {
            System.out.println("Row : Please input num less than 3");
            inputRownew();
        } else if ((col > 2 || col < 0) && row < 3) {
            System.out.println("Col : Please input num less than 3");
            inputColnew();
        } else if (col > 2 && row > 2) {
            System.out.println("Row and Col : Please input num less than 3");
            inputOX();
        } else if (XO[row][col] == ('O') || XO[row][col] == ('X')) {
            System.out.println("NOT Empty!!");
            inputOX();
        } else {
            addXo();
        }
    }

    private static void addXo() {
        if (sum % 2 == 0) {
            XO[row][col] = ('O');
            sum++;
        } else {
            XO[row][col] = ('X');
            sum++;
        }
    }

    private static void checkOwin() {
        if (XO[row1][col1] == ('O') && XO[row1][col1 + 1] == ('O') && XO[row1][col1 + 2] == ('O')
                || XO[row1][col1] == ('O') && XO[row1 + 1][col1] == ('O') && XO[row1 + 2][col1] == ('O')
                || XO[row1][col1] == ('O') && XO[row1 + 1][col1 + 1] == ('O') && XO[row1 + 2][col1 + 2] == ('O')
                || XO[row1 + 1][col1] == ('O') && XO[row1 + 1][col1 + 1] == ('O') && XO[row1 + 1][col1 + 2] == ('O')
                || XO[row1 + 2][col1] == ('O') && XO[row1 + 2][col1 + 1] == ('O') && XO[row1 + 2][col1 + 2] == ('O')
                || XO[row1][col1 + 1] == ('O') && XO[row1 + 1][col1 + 1] == ('O') && XO[row1 + 2][col1 + 1] == ('O')
                || XO[row1][col1 + 2] == ('O') && XO[row1 + 1][col1 + 2] == ('O') && XO[row1 + 2][col1 + 2] == ('O')
                || XO[row1][col1 + 2] == ('O') && XO[row1 + 1][col1 + 1] == ('O') && XO[row1 + 2][col1] == ('O')) {
            End = 1;
        }
    }

    private static void checkXwin() {
        if (XO[row1][col1] == ('X') && XO[row1][col1 + 1] == ('X') && XO[row1][col1 + 2] == ('X')
                || XO[row1][col1] == ('X') && XO[row1 + 1][col1] == ('X') && XO[row1 + 2][col1] == ('X')
                || XO[row1][col1] == ('X') && XO[row1 + 1][col1 + 1] == ('X') && XO[row1 + 2][col1 + 2] == ('X')
                || XO[row1 + 1][col1] == ('X') && XO[row1 + 1][col1 + 1] == ('X') && XO[row1 + 1][col1 + 2] == ('X')
                || XO[row1 + 2][col1] == ('X') && XO[row1 + 2][col1 + 1] == ('X') && XO[row1 + 2][col1 + 2] == ('X')
                || XO[row1][col1 + 1] == ('X') && XO[row1 + 1][col1 + 1] == ('X') && XO[row1 + 2][col1 + 1] == ('X')
                || XO[row1][col1 + 2] == ('X') && XO[row1 + 1][col1 + 2] == ('X') && XO[row1 + 2][col1 + 2] == ('X')
                || XO[row1][col1 + 2] == ('X') && XO[row1 + 1][col1 + 1] == ('X') && XO[row1 + 2][col1] == ('X')) {
            End = 2;
        }
    }

    private static void checkDraw() {
        if (sum == 9 && End == 0) {
            End = 3;
        }
    }

    private static void printOwin() {
        if (End == 1) {
            System.out.println("O Win");
        }
    }

    private static void printXwin() {
        if (End == 2) {
            System.out.println("X Win");
        }
    }

    private static void printDraw() {
        if (End == 3) {
            System.out.println("Draw");
        }
    }

    public static void main(String[] args) {
        showSay();
        inputTable(XO);
        showTable();
        while (End == 0) {
            inputOX();
            showTable();
            checkXwin();
            checkDraw();
            if (End != 0) {
                break;
            }
            inputOX();
            showTable();
            checkOwin();
            checkDraw();
        }
        printOwin();
        printXwin();
        printDraw();
    }

}
